function updateall
    if command -v apt >/dev/null 2>&1
        sudo apt update -y
        sudo apt full-upgrade -y
    else
        sudo pacman -Syyu
        yay -Syu
    end
    cargo install-update -a
end
