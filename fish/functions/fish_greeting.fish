function fish_greeting
    set -g fish_greeting \n(printf (_ '\t\t\t%sRESHAPE THE WORLD%s\n\n') (set_color green) (set_color normal))

    if set -q fish_private_mode
        set -l line (_ "fish is running in private mode, history will not be persisted.")
        if set -q fish_greeting[1]
            set -g fish_greeting $fish_greeting\n$line
        else
            set -g fish_greeting $line
        end
    end

    test -n "$fish_greeting"
    and echo $fish_greeting
end
